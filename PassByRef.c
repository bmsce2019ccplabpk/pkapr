#include<stdio.h>
void swap(int *a, int *b)
{
  printf("The original values are: a=%d and b=%d\n", a, b);
  a=a+b;
  b=a-b;
  a=a-b;
}
int main()
{
  int a, b;
  printf("Enter two numbers to swap: \n");
  scanf("%d%d", &a, &b);
  swap(&a, &b);
  printf("The swapped values are: a=%d and b=%d\n", a, b);
  return 0;
}
#include<stdio.h>
float calc(float);
void display(float, float);
int main()
{ float r, area;
  printf("Enter the radius: ");
  scanf("%f", &r);
  area=calc(r);
  display(r, area);
  return 0;
}

float calc(float rad)
{ float ar;
  ar=3.14159*rad*rad;
  return ar;
}

void display(float r, float area)
{ printf("The area of circle with radius %f is %f", r, area);
}
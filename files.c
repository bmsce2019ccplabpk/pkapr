#include<stdio.h>
int main()
{
    FILE *fp; char ch;
    fp=fopen("file.txt", "w");
    printf("Enter something...\n");
    while((ch=getchar())!=EOF)
    {
        putc(ch, fp);
    }
    fclose(fp);
    fp=fopen("file.txt", "r");
    printf("Printing from file...\n");
    while((ch=getc(fp))!=EOF)
    {
        printf("%c", ch);
    }
    printf("\n");
    fclose(fp);
    return 0;
}